#include <ntifs.h>

#include "kutil.hpp"
#include "logging/logging.hpp"
#include "misc.hpp"
#include "paging/paging.hpp"
#include "mem/mem.h"

//void DriverUnload(
//    _DRIVER_OBJECT* DriverObject)
//{
//    UNREFERENCED_PARAMETER(DriverObject);
//    log_ok("unloaded\n");
//}

#pragma warning(disable : 6387)
#pragma warning(disable : 4100)

enum OPERATION {
    init,
    read,
    write,
    modbase
};

struct ayylmao {
    u64 operation;
    PVOID from;
    PVOID to;
    u64 length;
    //common persisting
    u64 pid;
};

void GetModuleBaseByPidAndName(ayylmao* shmem) {
    PEPROCESS peproc;
    if (PsLookupProcessByProcessId((HANDLE)shmem->pid, &peproc) != STATUS_SUCCESS) {
        log_ok("fail 1\n");
        return;
    }
    auto ppeb = PsGetProcessPeb(peproc);
    ObDereferenceObject(peproc);
    if (!ppeb) {
        log_ok("fail 2\n");
        return;
    }

    PEB peb;
    SIZE_T temp;
    ReadProcessMemory(
        shmem->pid,
        ppeb,
        &peb,
        sizeof(PEB),
        &temp
    );
    if (!peb.LoaderData) {
        log_ok("fail 3\n");
        return;
    }
    PEB_LDR_DATA module_list_entry;
    ReadProcessMemory(
        shmem->pid,
        peb.LoaderData,
        &module_list_entry,
        sizeof(module_list_entry),
        &temp
    );

    auto first_entry = (void*)module_list_entry.InMemoryOrderModuleList.Flink;
    unsigned char* current_entry;
    ReadProcessMemory(
        shmem->pid,
        first_entry,
        &current_entry,
        sizeof(current_entry),
        &temp
    );

    WCHAR full_file_name[MAX_PATH];
    ULONGLONG module_base;
    ULONGLONG file_name_ptr;


    while (current_entry != first_entry)
    {
        ReadProcessMemory(
            shmem->pid,
            (unsigned char*)(current_entry)+0x40,
            &file_name_ptr,
            sizeof(file_name_ptr),
            &temp
        ); // read full module unicode_string structure.

        ReadProcessMemory(
            shmem->pid,
            (void*)file_name_ptr,
            full_file_name,
            MAX_PATH,
            &temp
        ); // read full file path.

        ReadProcessMemory(
            shmem->pid,
            (unsigned char*)(current_entry)+0x20,
            &module_base,
            sizeof(module_base),
            &temp
        );

        if (wcsstr(full_file_name, (wchar_t*)shmem->from))
        {
            shmem->to = reinterpret_cast<void*>(module_base);
            //log_ok("success\n");
            return;
        }

        ReadProcessMemory(
            shmem->pid,
            current_entry,
            &current_entry,
            sizeof(current_entry),
            &temp
        );
    }


}

OB_PREOP_CALLBACK_STATUS PobPreOperationCallback(
    PVOID RegistrationContext,
    POB_PRE_OPERATION_INFORMATION OperationInformation)
{
    if (OperationInformation->Parameters->CreateHandleInformation.OriginalDesiredAccess == (PROCESS_SET_QUOTA | PROCESS_TERMINATE | PROCESS_CREATE_THREAD | PROCESS_VM_WRITE)) {
        //log_ok("Callback entry: %p \n", OperationInformation->Parameters->CreateHandleInformation.OriginalDesiredAccess);
        SIZE_T byte_out;
        auto proc = PsGetCurrentProcess();
        if (!proc) goto EXIT;
        auto ppeb = PsGetProcessPeb(proc); //literally does PEPROCESS->Peb
        if (!ppeb) goto EXIT;
        ayylmao* shmem = (ayylmao*)ppeb->OemCodePageData;
        if (!shmem) goto EXIT;
        //log_ok("operation: %d\n", shmem->operation);
        switch (shmem->operation)
        {
        case OPERATION::init:
            break;

        case OPERATION::read:
            //log_ok("read operation\n");
            ReadProcessMemory(shmem->pid, shmem->from, shmem->to, shmem->length, &byte_out);
            break;

        case OPERATION::write:
            //log_ok("write operation\n");
            WriteProcessMemory(shmem->pid, shmem->to, shmem->from, shmem->length, &byte_out);
            break;

        case OPERATION::modbase:
            //log_ok("modbase read\n");
            GetModuleBaseByPidAndName(shmem);
            break;

        default:
            break;
        }
    }
    EXIT:
    return OB_PREOP_SUCCESS;
}

bool set_callback(const char* modulename_, PVOID cb_handler_)
{
    auto jump_loc = find_pattern("tcpip.sys", "\xff\xe1", "xx");
    if (!jump_loc) {
        log_er("failed to find jump_loc\n");
    } else {
        log_ok("jump_loc: %p\n", jump_loc);
    }
    OB_OPERATION_REGISTRATION OperationReg[1];
    OperationReg->ObjectType = PsProcessType;
    OperationReg->Operations = OB_OPERATION_HANDLE_CREATE;
    OperationReg->PreOperation = (POB_PRE_OPERATION_CALLBACK)jump_loc;
    OperationReg->PostOperation = nullptr;

    UNICODE_STRING CBAltitude = { 0 };
    RtlInitUnicodeString(&CBAltitude, L"1339");

    OB_CALLBACK_REGISTRATION CallbackReg[1];
    CallbackReg->Version = OB_FLT_REGISTRATION_VERSION;
    CallbackReg->Altitude = CBAltitude;
    CallbackReg->RegistrationContext = (PVOID)cb_handler_;
    CallbackReg->OperationRegistrationCount = 1;
    CallbackReg->OperationRegistration = OperationReg;

    PVOID RegHandle;

    auto status1 = ObRegisterCallbacks(
        CallbackReg,
        &RegHandle);

    //ACCESS_DENIED if cb routine not in signed region
    if (!NT_SUCCESS(status1)) {
        log_er("Failed to register cb: %p\n", status1);
        return false;
    }
    return true;
}

extern "C" NTSTATUS
DriverEntry(
    _In_ PDRIVER_OBJECT DriverObject,
    _In_ PUNICODE_STRING RegistryPath)
{
    UNREFERENCED_PARAMETER(RegistryPath);
    UNREFERENCED_PARAMETER(DriverObject);
    //will crash kdmapper
    //DriverObject->DriverUnload = DriverUnload;

    log_ok("man-o-war-probe loaded\n");

    set_callback("tcpip.sys", PobPreOperationCallback);

    return STATUS_SUCCESS;
}
