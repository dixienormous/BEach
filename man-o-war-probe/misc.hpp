#pragma once

#include <ntddk.h>
#include "kutil.hpp"

using u64 = unsigned __int64;


typedef struct _KLDR_DATA_TABLE_ENTRY
{
    struct _LIST_ENTRY InLoadOrderLinks;                                    //0x0
    VOID* ExceptionTable;                                                   //0x10
    ULONG ExceptionTableSize;                                               //0x18
    VOID* GpValue;                                                          //0x20
    struct _NON_PAGED_DEBUG_INFO* NonPagedDebugInfo;                        //0x28
    VOID* DllBase;                                                          //0x30
    VOID* EntryPoint;                                                       //0x38
    ULONG SizeOfImage;                                                      //0x40
    struct _UNICODE_STRING FullDllName;                                     //0x48
    struct _UNICODE_STRING BaseDllName;                                     //0x58
    ULONG Flags;                                                            //0x68
    USHORT LoadCount;                                                       //0x6c
    union
    {
        USHORT SignatureLevel : 4;                                            //0x6e
        USHORT SignatureType : 3;                                             //0x6e
        USHORT Unused : 9;                                                    //0x6e
        USHORT EntireField;                                                 //0x6e
    } u1;                                                                   //0x6e
    VOID* SectionPointer;                                                   //0x70
    ULONG CheckSum;                                                         //0x78
    ULONG CoverageSectionSize;                                              //0x7c
    VOID* CoverageSection;                                                  //0x80
    VOID* LoadedImports;                                                    //0x88
    VOID* Spare;                                                            //0x90
    ULONG SizeOfImageNotRounded;                                            //0x98
    ULONG TimeDateStamp;                                                    //0x9c
}KLDR_DATA_TABLE_ENTRY, * PKLDR_DATA_TABLE_ENTRY;


auto find_pattern(
    const char* modulename_, const char* signature, const char* mask
) -> void*
{
    auto mod_base = kutils::driver::get_driver_base(modulename_);
    if (!mod_base) {
        log_er("Could not find mod_base\n");
        return  0;
    }
    const auto pe_header = kutils::pe::get_nt_header(mod_base);
    const auto section_count = pe_header->FileHeader.NumberOfSections;
    const auto section_header = IMAGE_FIRST_SECTION(pe_header);

    for (auto i = 0U; i < section_count; ++i)
    {
        const auto current_section = &section_header[i];
        if (!current_section) continue;

        if (current_section->Characteristics & IMAGE_SCN_MEM_EXECUTE
            && !(current_section->Characteristics & IMAGE_SCN_MEM_DISCARDABLE))
        {   
            const auto scan_base = reinterpret_cast<char*>(mod_base) + current_section->VirtualAddress;
            const auto scan_size = current_section->Misc.VirtualSize;
            const auto addr = kutils::signature::scan(scan_base, scan_size, signature, mask);

            if (!addr)
                continue;

            return addr;
        }
    }
    return nullptr;
}