#include "paging.hpp"
#include "../logging/logging.hpp"


PHYSICAL_ADDRESS get_pa_from_va(PVOID _paging_base_pa, PVOID _va) {


	if (_paging_base_pa == nullptr || _va == nullptr) {
		return PHYSICAL_ADDRESS{};
	}
	virt_addr_t va;
	va.value = _va;

	log_ok("pml4_index %d\n", va.field.pml4_index);
	log_ok("pdpt_index %d\n", va.field.pdpt_index);
	log_ok("pd_index %d\n", va.field.pd_index);
	log_ok("pt_index %d\n", va.field.pt_index);

	pml4e* base = reinterpret_cast<pml4e*>
		(MmGetVirtualForPhysical(PHYSICAL_ADDRESS{ .QuadPart=(LONGLONG)_paging_base_pa }));
	pdpte* pdpt = reinterpret_cast<pdpte*>(MmGetVirtualForPhysical(PHYSICAL_ADDRESS{ .QuadPart = (static_cast<LONGLONG>(base[va.field.pml4_index].field.pfn << 12)) }));
	pde* pd = reinterpret_cast<pde*>(MmGetVirtualForPhysical(PHYSICAL_ADDRESS{ .QuadPart = (static_cast<LONGLONG>(pdpt[va.field.pdpt_index].field.pfn << 12 ))}));
	pte* pt = reinterpret_cast<pte*>(MmGetVirtualForPhysical(PHYSICAL_ADDRESS{ .QuadPart = (static_cast<LONGLONG>(pd[va.field.pd_index].field.pfn << 12 ))}));
	//PVOID page_base_pa =
	//	MmGetVirtualForPhysical(PHYSICAL_ADDRESS{ .QuadPart = (static_cast<LONGLONG>(pt[va.field.pt_index].field.pfn << 12)) });


	PVOID page_base_pa =
		reinterpret_cast<PVOID>(pt[va.field.pt_index].field.pfn << 12);

	LONGLONG translated_pa =
		reinterpret_cast<LONGLONG>(page_base_pa) + va.field.offset;


	log_ok("page_base_pa: 0x%p\n", (PVOID)page_base_pa);
	log_ok("translated PA: 0x%p\n", (PVOID)translated_pa);
	//log_debug_ok("reading translated PA: %d", MmGetVirtualForPhysical(PHYSICAL_ADDRESS{ .QuadPart = translated_pa }));
	
	return PHYSICAL_ADDRESS{ .QuadPart = translated_pa };

}